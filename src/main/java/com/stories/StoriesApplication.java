package com.stories;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;

@SpringBootApplication(exclude = FlywayAutoConfiguration.class)
public class StoriesApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoriesApplication.class, args);
    }

}
